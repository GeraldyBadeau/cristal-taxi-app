import { Platform, Dimensions, I18nManager } from 'react-native';
import { AppleButton } from '@invertase/react-native-apple-authentication';
import invert from 'invert-color';
import TNColor from './Core/truly-native/TNColor';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const lightColorSet = {
  mainThemeBackgroundColor: '#ffffff',
  mainTextColor: '#434343',
  mainSubtextColor: '#7e7e7e',
  secondaryMaintextColor: '#595959',
  secondarySubtextColor: '#a2a2a2',
  mainThemeForegroundColor: '#434343',
  hairlineColor: '#e0e0e0',
  grey0: '#eaeaea',
  grey1: '#cecdce',
  grey2: '#efefef',
  grey3: '#f5f5f5',
  grey4: '#c4c4c4',
  grey6: '#d6d6d6',
  grey5: '#b3b3b3',
  grey9: '#939393',
  whiteSmoke: '#f5f5f5',
  grey: 'grey',
  inputBgColor: '#E8E8E8',
  blue: '#2d73ed',
};

const darkColorSet = {
  mainThemeBackgroundColor: TNColor('#ffffff'),
  mainTextColor: TNColor('#434343'),
  mainSubtextColor: TNColor('#7e7e7e'),
  secondaryMaintextColor: TNColor('#595959'),
  secondarySubtextColor: TNColor('#a2a2a2'),
  mainThemeForegroundColor: '#434343',
  hairlineColor: TNColor('#e0e0e0'),
  grey0: TNColor('#eaeaea'),
  grey1: TNColor('#cecdce'),
  grey2: TNColor('#efefef'),
  grey3: TNColor('#f5f5f5'),
  grey4: TNColor('#c4c4c4'),
  grey6: TNColor('#d6d6d6'),
  grey5: TNColor('#b3b3b3'),
  grey9: TNColor('#939393'),
  whiteSmoke: TNColor('#e0e0e0'),
  grey: 'grey',
  inputBgColor: TNColor('#E8E8E8'),
  blue: '#2d73ed',
};

const colorSet = {
  light: lightColorSet,
  dark: darkColorSet,
  'no-preference': lightColorSet,
};

const navLight = {
  backgroundColor: '#fff',
  fontColor: '#000',
  headerStyleColor: '#E8E8E8',
  iconBackground: '#F4F4F4',
};

const navDark = {
  backgroundColor: invert('#fff'),
  fontColor: invert('#000'),
  headerStyleColor: invert('#E8E8E8'),
  iconBackground: invert('#e6e6f2'),
};

const navThemeConstants = {
  light: navLight,
  dark: navDark,
  'no-preference': navLight,
};

const fontFamily = {
  system: Platform.OS === 'ios' ? 'SFProDisplay-Semibold' : '0',
  boldFont: 'Oswald-Bold',
  semiBoldFont: 'Oswald-SemiBold',
  regularFont: 'Oswald-Regular',
  mediumFont: 'Oswald-Medium',
  lightFont: 'Oswald-Light',
  extraLightFont: 'Oswald-ExtraLight',
};

const iconSet = {
  home: require('../assets/icons/home-icon.png'),
  home_android: require('../assets/icons/home-icon-24.png'),
  rightArrow: require('../assets/icons/right-arrow.png'),
  visaPay: require('../assets/icons/visa.png'),
  americanExpress: require('../assets/icons/american-express.png'),
  dinersClub: require('../assets/icons/diners-club.png'),
  discover: require('../assets/icons/discover.png'),
  jcb: require('../assets/icons/jcb.png'),
  mastercard: require('../assets/icons/mastercard.png'),
  unionpay: require('../assets/icons/unionpay.png'),
  cash: require('../assets/icons/cash.png'),
  tick: require('../assets/icons/tick.png'),
  plus: require('../assets/icons/plus.png'),
  back: require('../assets/icons/back.png'),
  menu: require('../assets/icons/menu.png'),
  call: require('../assets/icons/call.png'),
  pin: require('../assets/icons/pin.png'),
  userFilled: require('../assets/icons/user-filled.png'),
  mapLocation: require('../assets/map-location.png'),
  driver: require('../assets/driver.png'),
  markerImage: require('./assets/icons/marker.png'),
  menuHamburger: require('./CoreAssets/hamburger-menu-icon.png'),
  logo: require('../assets/applogo.png'),
  backArrow: require('../assets/icons/backArrow.png'),
  userAvatar: require('../assets/icons/default-avatar.jpg'),
  close: require('./CoreAssets/close-x-icon.png'),
};

const appIcon = {
  style: {
    width: 25,
    height: 25,
  },
  images: {
    accountDetail: require('../assets/icons/account-detail.png'),
    settings: require('../assets/icons/settings.png'),
    contactUs: require('../assets/icons/contact-us.png'),
    delivery: require('../assets/icons/delivery.png'),
    favoriteRestaurant: require('../assets/icons/love.png'),
  },
};

const fontSet = {
  xxlarge: 40,
  xlarge: 30,
  large: 25,
  middle: 20,
  normal: 16,
  small: 13,
  xsmall: 11,
  title: 30,
  content: 20,
};

const loadingModal = {
  color: '#FFFFFF',
  size: 20,
  overlayColor: 'rgba(0,0,0,0.5)',
  closeOnTouch: false,
  loadingType: 'Spinner', // 'Bubbles', 'DoubleBounce', 'Bars', 'Pulse', 'Spinner'
};

const sizeSet = {
  buttonWidth: '65%',
  inputWidth: '80%',
  radius: 25,
};

const styleSet = {
  searchBar: {
    container: {
      marginLeft: Platform.OS === 'ios' ? 30 : 0,
      backgroundColor: 'transparent',
      borderBottomColor: 'transparent',
      borderTopColor: 'transparent',
      flex: 1,
    },
    input: {
      backgroundColor: colorSet.inputBgColor,
      borderRadius: 10,
    },
  },
  backArrowStyle: {
    resizeMode: 'contain',
    tintColor: '#434343',
    width: 25,
    height: 25,
    marginTop: Platform.OS === 'ios' ? 50 : 20,
    marginLeft: 10,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
};

const appleButtonStyle = {
  dark: AppleButton?.Style?.WHITE,
  light: AppleButton?.Style?.BLACK,
  'no-preference': AppleButton?.Style?.WHITE,
};

const StyleDict = {
  fontFamily,
  colorSet,
  navThemeConstants,
  fontSet,
  sizeSet,
  iconSet,
  appIcon,
  styleSet,
  loadingModal,
  WINDOW_WIDTH,
  WINDOW_HEIGHT,
  appleButtonStyle,
};

export default StyleDict;
