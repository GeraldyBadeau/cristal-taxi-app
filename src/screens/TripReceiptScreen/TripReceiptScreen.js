import React, { useLayoutEffect } from 'react';
import { View, ScrollView, Text, Image } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import dynamicStyles from './styles';
import AppConfig from '../../config';

export default function TripReceipt(props) {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const trip = props.route?.params?.trip;

  const dateOptions = {
    weekday: 'short',
    year: 'numeric',
    month: 'short',
    day: 'numeric',
  };
  const date = new Date(trip?.tripEndTime);

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerStyle: styles.navHeaderContainer,
    });
  }, [props.navigation]);

  return (
    <ScrollView style={styles.container}>
      <View style={[styles.rowsContainer, styles.messageContainer]}>
        <View style={styles.messageDescriptionContainer}>
          <Text style={styles.subtitle}>
            {date.toLocaleDateString('en-US', dateOptions)}
          </Text>
          <Text style={styles.mainTitle}>{'Thanks for riding,'}</Text>
          <Text style={styles.mainTitle}>{`${
            trip?.passenger?.firstName ?? ''
          } ${trip?.passenger?.lastName ?? ''}`}</Text>
        </View>
        <View style={styles.messageImageContainer}>
          <Image
            source={{
              uri: trip?.driver?.carAvatar ?? AppConfig.defaultCarAvatar,
            }}
            style={styles.messageImage}
            resizeMode={'contain'}
          />
        </View>
      </View>
      <View style={[styles.rowsContainer, styles.totalContainer]}>
        <View style={styles.amountLabelContainer}>
          <Text style={styles.mainTitle}>{'Total'}</Text>
        </View>
        <View style={styles.amountContainer}>
          <Text
            style={
              styles.mainTitle
            }>{`${trip.price} ${AppConfig.displayCurrencyTitle}`}</Text>
        </View>
      </View>
      <View style={[styles.rowsContainer, styles.tripFareTitleContainer]}>
        <View style={styles.amountLabelContainer}>
          <Text style={styles.subtitle}>{'Trip Fare'}</Text>
        </View>
        <View style={styles.amountContainer}>
          <Text
            style={
              styles.subtitle
            }>{`${trip.price} ${AppConfig.displayCurrencyTitle}`}</Text>
        </View>
      </View>
      <View style={[styles.rowsContainer, styles.tripFareTitleContainer]}>
        <View style={styles.amountLabelContainer}>
          <Text style={styles.subtitleBold}>{'Subtotal'}</Text>
          <Text style={styles.subtitle}>{'Tolls, Surcharge, and Fees'}</Text>
        </View>
        <View style={styles.amountContainer}>
          <Text
            style={
              styles.subtitleBold
            }>{`${trip.price} ${AppConfig.displayCurrencyTitle}`}</Text>
          <Text
            style={
              styles.subtitle
            }>{`${trip.price} ${AppConfig.displayCurrencyTitle}`}</Text>
        </View>
      </View>
    </ScrollView>
  );
}
