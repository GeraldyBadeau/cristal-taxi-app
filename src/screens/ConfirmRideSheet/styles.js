import { StyleSheet } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    container: {
      // flex: 1,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      height: 235,
    },
    titleContainer: {
      paddingTop: 10,
      paddingBottom: 20,
      borderBottomColor: AppStyles.colorSet[colorScheme].blue,
      borderBottomWidth: 1,
    },
    title: {
      paddingHorizontal: 10,
      fontSize: 20,
      fontWeight: '600',
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    imageContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 135,
    },
    image: {
      height: 100,
      width: 100,
    },
  });
};

export default dynamicStyles;
