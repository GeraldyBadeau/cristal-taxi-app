import React, { useLayoutEffect } from 'react';
import { connect } from 'react-redux';
import AppStyles from '../../DynamicAppStyles';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import { logout, setUserData } from '../../Core/onboarding/redux/auth';
import { TNTouchableIcon } from '../../Core/truly-native';
import { IMUserProfileComponent } from '../../Core/profile';
import { authManager } from '../../Core/onboarding/utils/api';
import { useColorScheme } from 'react-native-appearance';
import MenuIcon from '../../components/MenuIcon/MenuIcon';

const ProfileScreen = (props) => {
  const { navigation } = props;
  const appConfig = props.route.params?.appConfig;
  let colorScheme = useColorScheme();

  useLayoutEffect(() => {
    const currentTheme = AppStyles.navThemeConstants[colorScheme];
    navigation.setOptions({
      title: IMLocalized('My Profile'),
      headerStyle: {
        backgroundColor: currentTheme.backgroundColor,
      },
      headerTitleStyle: {
        color: currentTheme.fontColor,
      },
      headerLeft: renderNavHeaderLeft,
    });
  }, [navigation]);

  const renderNavHeaderLeft = () => {
    return <MenuIcon onPress={() => navigation.openDrawer()} />;
  };

  const onAccountDetailsPress = () => {
    navigation.navigate('AccountDetails', {
      appStyles: AppStyles,
      form: appConfig.editProfileFields,
      screenTitle: IMLocalized('Edit Profile'),
    });
  };

  const onSettingsPress = () => {
    navigation.navigate('Settings', {
      appStyles: AppStyles,
      form: appConfig.userSettingsFields,
      screenTitle: IMLocalized('Settings'),
    });
  };

  const onBlockedUsersPress = () => {
    navigation.navigate('BlockedUsers', {
      appStyles: AppStyles,
      screenTitle: IMLocalized('BlockedUsers'),
    });
  };

  const onContactUsPress = () => {
    navigation.navigate('ContactUs', {
      appStyles: AppStyles,
      screenTitle: IMLocalized('Contact Us'),
      form: appConfig.contactUsFields,
      phone: appConfig.contactUsPhoneNumber,
    });
  };

  const onUpdateUser = (newUser) => {
    props.setUserData({ user: newUser });
  };

  const onLogout = () => {
    authManager.logout(props.user);
    props.logout();
    props.navigation.reset({
      index: 0,
      routes: [
        {
          name: 'LoadScreen',
          params: { appStyles: AppStyles, appConfig },
        },
      ],
    });
  };

  const menuItems = [
    {
      title: IMLocalized('Account Details'),
      icon: require('../../CoreAssets/account-details-icon.png'),
      tintColor: '#6b7be8',
      onPress: onAccountDetailsPress,
    },
    {
      title: IMLocalized('Settings'),
      icon: require('../../CoreAssets/settings-icon.png'),
      tintColor: '#777777',
      onPress: onSettingsPress,
    },
    {
      title: IMLocalized('Contact Us'),
      icon: require('../../CoreAssets/contact-us-icon.png'),
      tintColor: '#9ee19f',
      onPress: onContactUsPress,
    },
  ];

  return (
    <IMUserProfileComponent
      user={props.user}
      onUpdateUser={onUpdateUser}
      onLogout={onLogout}
      menuItems={menuItems}
      appStyles={AppStyles}
    />
  );
};

const mapStateToProps = ({ auth }) => ({
  user: auth.user,
});

export default connect(mapStateToProps, { logout, setUserData })(ProfileScreen);
