import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { useColorScheme } from 'react-native-appearance';
import dynamicStyles from './styles';
import AppStyle from '../../../DynamicAppStyles';
import HistoryMap from '../HistoryMap';
import AppConfig from '../../../config';

export default function TripHistoryItem({ item }) {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const navigation = useNavigation();

  const date = new Date(item?.tripEndTime);
  const timeOptions = { hour: '2-digit', minute: '2-digit' };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation.navigate('TripsDetail', { trip: item })}
        style={styles.detialContainer}>
        <View style={styles.dateContainer}>
          <Text style={styles.date}>{`${date.toLocaleDateString(
            'fr-FR',
          )}, ${date.toLocaleTimeString('fr-FR', timeOptions)}`}</Text>
          <Text style={styles.rideType}>{`${item?.driver?.carName ?? ''} ${
            item?.driver?.carNumber ?? ''
          }`}</Text>
        </View>

        {/* <View style={styles.priceContainer}>
          <Text
            style={
              styles.price
            }>{`${item?.price} ${AppConfig.displayCurrencyTitle}`}</Text>
        </View> */}
        <View style={styles.iconContainer}>
          <Image style={styles.icon} source={AppStyle.iconSet.rightArrow} />
        </View>
      </TouchableOpacity>
      <View style={styles.mapContainer}>
        <HistoryMap
          origin={item.pickup}
          destination={item.dropoff}
          coordinates={item.tripCoordinates}
          zoomEnabled={false}
          scrollEnabled={false}
        />
      </View>
    </View>
  );
}
