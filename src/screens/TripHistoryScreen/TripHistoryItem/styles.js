import { StyleSheet } from 'react-native';
import AppStyles from '../../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    container: {
      width: '100%',
      paddingVertical: 10,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    detialContainer: {
      flexDirection: 'row',
      width: '100%',
      paddingVertical: 10,
    },
    dateContainer: {
      flex: 7,
      justifyContent: 'center',
      paddingVertical: 10,
      paddingLeft: 20,
    },
    date: {
      fontSize: 14,
      fontWeight: '500',
      paddingBottom: 5,
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    rideType: {
      fontSize: 12,
      fontWeight: '300',
      color: AppStyles.colorSet[colorScheme].secondaryMaintextColor,
    },
    priceContainer: {
      flex: 1.7,
      paddingVertical: 10,
      alignItems: 'flex-end',
    },
    price: {
      fontSize: 16,
      fontWeight: '300',
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    iconContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    icon: {
      width: 13,
      height: 14,
      tintColor: AppStyles.colorSet[colorScheme].grey1,
    },
    mapContainer: {
      width: '100%',
      height: 190,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
  });
};

export default dynamicStyles;
