import React, { useLayoutEffect, useEffect, useState } from 'react';
import { FlatList, View } from 'react-native';
import { useSelector } from 'react-redux';
import { useColorScheme } from 'react-native-appearance';
import TripHistoryItem from './TripHistoryItem/TripHistoryItem';
import MenuIcon from '../../components/MenuIcon/MenuIcon';
import { tripsAPIManager } from '../../api';
import dynamicStyles from './styles';
import AppStyles from '../../DynamicAppStyles';
import { TNActivityIndicator, TNEmptyStateView } from '../../Core/truly-native';
import { IMLocalized } from '../../Core/localization/IMLocalization';

export default function TripHistoryScreen({ navigation }) {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const currentUser = useSelector(({ auth }) => auth.user);

  const [loading, setLoading] = useState(false);
  const [trips, setTrips] = useState([]);

  const emptyStateConfig = {
    title: IMLocalized('No Rides'),
    description: IMLocalized(
      'You have not ordered any rides yet. All your completed trips will appear here.',
    ),
  };

  useLayoutEffect(() => {
    const currentTheme = AppStyles.navThemeConstants[colorScheme];
    navigation.setOptions({
      headerLeft: renderNavHeaderLeft,
      headerStyle: [
        styles.navHeaderContainer,
        {
          backgroundColor: currentTheme.backgroundColor,
        },
      ],
      headerTitleStyle: {
        color: currentTheme.fontColor,
      },
    });
  }, [navigation]);

  useEffect(() => {
    setLoading(true);
    const unsubscribe = tripsAPIManager.subscribeTripHistory(
      currentUser.id,
      onHistoryUpdate,
    );
    return unsubscribe;
  }, []);

  const onHistoryUpdate = (data) => {
    setTrips(data);
    setLoading(false);
  };

  const renderNavHeaderLeft = () => {
    return <MenuIcon onPress={() => navigation.openDrawer()} />;
  };

  const renderTripItem = ({ item, index }) => {
    return <TripHistoryItem key={item.id ?? `${index}`} item={item} />;
  };

  const renderEmptyState = () => {
    return (
      <View style={styles.emptyViewContainer}>
        <TNEmptyStateView
          appStyles={AppStyles}
          emptyStateConfig={emptyStateConfig}
        />
      </View>
    );
  };

  if (loading) {
    return (
      <View style={styles.container}>
        <TNActivityIndicator appStyles={AppStyles} />
      </View>
    );
  }

  return (
    <FlatList
      style={styles.container}
      data={trips}
      keyExtractor={(item, index) => item.id ?? `${index}`}
      renderItem={renderTripItem}
      ListEmptyComponent={renderEmptyState}
    />
  );
}
