import { StyleSheet } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    container: {
      paddingTop: 12,
      width: '100%',
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    imageContainer: {
      width: '100%',
      height: 120,
      justifyContent: 'center',
      alignItems: 'center',
    },
    image: {
      height: 150,
      width: 160,
      resizeMode: 'contain',
    },
    descriptionContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingVertical: 20,
      paddingLeft: 20,
    },
    middleContainer: {
      flex: 1,
      marginHorizontal: 10,
    },
    type: {
      fontWeight: 'bold',
      fontSize: 25,
      marginBottom: 5,
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    numberOfPassenger: {
      fontWeight: 'bold',
      fontSize: 12,
      marginBottom: 5,
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    time: {
      color: AppStyles.colorSet[colorScheme].mainSubtextColor,
      fontSize: 18,
      paddingBottom: 3,
    },
    rightContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingRight: 5,
      paddingVertical: 5,
    },
    price: {
      fontWeight: 'bold',
      fontSize: 21,
      marginLeft: 5,
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
  });
};

export default dynamicStyles;
