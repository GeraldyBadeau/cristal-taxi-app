import React, { useRef, useLayoutEffect } from 'react';
import { View, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useColorScheme } from 'react-native-appearance';
import HistoryMap from '../TripHistoryScreen/HistoryMap';
import dynamicStyles from './styles';
import AppConfig from '../../config';

export default function TripsHistoryDetailScreen(props) {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const trip = props.route?.params?.trip;
  const { navigation } = props;

  const date = new Date(trip?.tripEndTime);
  const timeOptions = { hour: '2-digit', minute: '2-digit' };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerStyle: styles.navHeaderContainer,
    });
  }, [navigation]);

  const onReceiptPress = () => {
    navigation.navigate('TripReceipt', { trip });
  };

  return (
    <View style={styles.container}>
      <View style={styles.mapContainer}>
        <HistoryMap
          origin={trip.pickup}
          destination={trip.dropoff}
          coordinates={trip.tripCoordinates}
        />
      </View>
      <View style={styles.detialContainer}>
        <View style={styles.dateContainer}>
          <Text style={styles.date}>{`${date.toLocaleDateString(
            'en-GB',
          )}, ${date.toLocaleTimeString('fr-FR', timeOptions)}`}</Text>
          <Text style={styles.rideType}>{`${trip?.driver?.carName ?? ''} ${
            trip?.driver?.carNumber ?? ''
          }`}</Text>
        </View>

        {/* <View style={styles.priceContainer}>
          <Text
            style={
              styles.price
            }>{`${trip?.price} ${AppConfig.displayCurrencyTitle}`}</Text>
        </View> */}
      </View>

      <View style={styles.routeContainer}>
        <View style={styles.routeIindicatorContainer}>
          <View style={styles.circle} />

          <View style={styles.line} />

          <View style={styles.square} />
        </View>
        <View style={styles.routeDescriptionContainer}>
          <Text style={styles.routeTitle}>{trip?.pickup?.title}</Text>
          <Text style={styles.routeTitle}>{trip?.dropoff?.title}</Text>
        </View>
        {/* <View style={styles.receiptContainer}>
          <TouchableOpacity
            onPress={onReceiptPress}
            style={styles.buttonContainer}>
            <Text style={styles.receiptTitle}>{'Receipt'}</Text>
          </TouchableOpacity>
        </View> */}
      </View>
    </View>
  );
}
