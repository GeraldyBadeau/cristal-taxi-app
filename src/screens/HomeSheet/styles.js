import { StyleSheet } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    container: {
      paddingTop: 12,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    whereTitleBox: {
      backgroundColor: AppStyles.colorSet[colorScheme].grey2,
      margin: 10,
      paddingHorizontal: 15,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    whereTitleText: {
      fontSize: 20,
      fontWeight: '600',
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    searchContainer: {
      width: 100,
      height: '70%',
      padding: 10,
      justifyContent: 'center',
      alignItems: 'center',
      borderLeftWidth: 1,
      borderLeftColor: AppStyles.colorSet[colorScheme].grey1,
    },
    searchTitle: {
      fontSize: 16,
      fontWeight: '400',
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    locationItemContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      padding: 20,
      borderBottomWidth: 1,
      borderColor: AppStyles.colorSet[colorScheme].grey0,
    },
    iconContainer: {
      backgroundColor: AppStyles.colorSet[colorScheme].grey5,
      padding: 10,
      borderRadius: 25,
    },
    destinationText: {
      marginLeft: 10,
      fontWeight: '500',
      fontSize: 16,
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    secondaryLocationText: {
      fontWeight: '400',
      color: AppStyles.colorSet[colorScheme].secondarySubtextColor,
      paddingTop: 3,
    },
  });
};

export default dynamicStyles;
