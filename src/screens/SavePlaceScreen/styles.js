import { StyleSheet } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    inputContainer: {
      width: '100%',
      paddingLeft: 20,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    inputTitleContainer: {
      paddingVertical: 5,
    },
    inputTitle: {
      fontSize: 18,
      color: AppStyles.colorSet[colorScheme].grey5,
    },
    textInputContainer: {
      paddingVertical: 2,
      borderBottomColor: AppStyles.colorSet[colorScheme].grey0,
      borderBottomWidth: 1,
      paddingBottom: 20,
    },
    textInput: {
      fontSize: 18,
      paddingBottom: 0,
      width: '100%',
      fontWeight: '300',
    },
    saveTitleContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      width: '85%',
      height: 54,
      backgroundColor: AppStyles.colorSet[colorScheme].grey5,
      alignSelf: 'center',
      marginTop: '45%',
    },
    activeSaveTitleContainer: {
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeForegroundColor,
    },
    saveTitle: {
      fontSize: 20,
      fontWeight: '400',
      color: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
  });
};

export default dynamicStyles;
