import { StyleSheet, Dimensions } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

const cardInputFontSize = 16;

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    navHeaderContainer: {
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    optionsTitleContainer: {
      width: '100%',
      paddingLeft: 20,
      height: 60,
      paddingVertical: 10,
    },
    optionsTitle: {
      color: AppStyles.colorSet[colorScheme].mainTextColor,
      fontSize: cardInputFontSize,
      paddingLeft: 7,
      fontWeight: 'bold',
    },
    optionsContainer: {
      width: '100%',
      borderBottomColor: AppStyles.colorSet[colorScheme].grey6,
      borderBottomWidth: 0.5,
      borderTopColor: AppStyles.colorSet[colorScheme].grey6,
      borderTopWidth: 0.5,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    paymentMethodContainer: {
      flexDirection: 'row',
      borderBottomColor: AppStyles.colorSet[colorScheme].grey6,
      borderBottomWidth: 0.5,
      paddingLeft: 20,
      height: 60,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    addNewCardContainer: {
      flexDirection: 'row',
      borderBottomColor: AppStyles.colorSet[colorScheme].subHairlineColor,
      borderBottomWidth: 0.5,
    },
    addNewCardTitleContainer: {
      flex: 4,
      justifyContent: 'center',
      marginVertical: 10,
    },
    addNewCardTitle: {
      color: AppStyles.colorSet[colorScheme].mainSubtextColor,
      fontSize: cardInputFontSize,
      paddingLeft: 7,
    },
  });
};

export default dynamicStyles;
