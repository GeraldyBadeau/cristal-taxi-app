import React, { useEffect, useState } from 'react';
import { Text, View, Image, TouchableOpacity, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { useColorScheme } from 'react-native-appearance';
import dynamicStyles from './styles';
import AppStyles from '../../DynamicAppStyles';
import {
  setSelectedPaymentMethod,
  removeFromPaymentMethods,
} from '../../redux';
import { IMLocalized } from '../../Core/localization/IMLocalization';
import PaymentOptionItem from '../../components/PaymentOptionItem/PaymentOptionItem';
import { userAPIManager } from '../../Core/api';
import { setUserData } from '../../Core/onboarding/redux/auth';

function PaymentOptions(props) {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);
  const { onAddNewCard, onPaymentMethodLongPress } = props;

  const dispatch = useDispatch();
  const paymentMethods = useSelector(({ payment }) => payment.paymentMethods);
  const selectedPaymentMethod = useSelector(
    ({ payment }) => payment.selectedPaymentMethod,
  );
  const currentUser = useSelector(({ auth }) => auth.user);

  const [selectedMethodIndex, setSelectedMethodIndex] = useState(0);

  useEffect(() => {
    if (paymentMethods?.length > 0 && selectedPaymentMethod) {
      const selectedIndex = paymentMethods.findIndex(
        (paymentMethod) => paymentMethod.key === selectedPaymentMethod.key,
      );

      if (selectedIndex > -1) {
        setSelectedMethodIndex(selectedIndex);
        return;
      }
    }
  }, [selectedPaymentMethod]);

  const onPaymentMethodPress = (index, item) => {
    setSelectedMethodIndex(index);
    dispatch(setSelectedPaymentMethod(item));
    userAPIManager.updateUserData(currentUser.id, {
      defaultPaymentKey: item.key,
    });

    dispatch(
      setUserData({
        user: { ...currentUser, defaultPaymentKey: item.key },
      }),
    );
  };

  const onLongPress = (method) => {
    if (method?.key === 'cash') return;

    Alert.alert(
      IMLocalized('Remove card'),
      IMLocalized('This card will be removed from payment methods.'),
      [
        {
          text: IMLocalized('Remove'),
          onPress: () => onPaymentMethodLongPress(method),
          style: 'destructive',
        },
        {
          text: IMLocalized('Cancel'),
        },
      ],
      { cancelable: true },
    );
  };

  const renderCard = (item, index) => {
    const isLastItem = index === paymentMethods.length - 1;
    return (
      <PaymentOptionItem
        key={`${item?.key ?? index}`}
        index={index}
        isLastItem={isLastItem}
        selectedMethodIndex={selectedMethodIndex}
        item={item}
        onPress={onPaymentMethodPress}
        onLongPress={onLongPress}
        selectedIconSource={AppStyles.iconSet.tick}
      />
    );
  };

  return (
    <>
      <View style={styles.optionsContainer}>
        {paymentMethods.map(renderCard)}
      </View>
      <TouchableOpacity
        onPress={onAddNewCard}
        style={styles.paymentMethodContainer}>
        <View style={styles.addNewCardTitleContainer}>
          <Text style={styles.addNewCardTitle}>
            {IMLocalized('Add payment method')}
          </Text>
        </View>
      </TouchableOpacity>
    </>
  );
}

export default PaymentOptions;
