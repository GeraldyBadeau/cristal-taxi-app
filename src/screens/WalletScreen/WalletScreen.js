import React, { useLayoutEffect, useEffect, useRef } from 'react';
import { View, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import stripe from 'tipsi-stripe';
import { useColorScheme } from 'react-native-appearance';
import { userAPIManager, PaymentAPIManager } from '../../Core/api';
import PaymentRequestAPI from '../../Core/payment/api';
import { updatePaymentMethods, setSelectedPaymentMethod } from '../../redux';
import { setUserData } from '../../Core/onboarding/redux/auth';
import PaymentOptions from './PaymentOptions';
import dynamicStyles from './styles';
import appConfig from '../../config';
import AppStyles from '../../DynamicAppStyles';
import MenuIcon from '../../components/MenuIcon/MenuIcon';
import { IMLocalized } from '../../Core/localization/IMLocalization';

const options = {
  requiredBillingAddressFields: 'full',
  prefilledInformation: {
    billingAddress: {
      name: 'Marya Ken',
      line1: 'Canary Place',
      line2: '3',
      city: 'Macon',
      state: 'Georgia',
      country: 'US',
      postalCode: '31217',
    },
  },
};

export default function WalletScreen(props) {
  const { navigation, route } = props;
  const canGoBack = route.params?.canGoBack;

  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const dispatch = useDispatch();

  const currentUser = useSelector((state) => state.auth.user);
  const paymentMethods = useSelector(({ payment }) => payment.paymentMethods);

  const paymentMethodDataManager = useRef(new PaymentAPIManager(appConfig));
  const paymentRequestAPI = useRef(new PaymentRequestAPI(appConfig));
  const stripeCustomerID = useRef(null);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: renderNavHeaderLeft,
      headerStyle: styles.navHeaderContainer,
    });
  }, [navigation]);

  useEffect(() => {
    setStripeCustomerId();

    const unsubscribePaymentMethods = paymentMethodDataManager.current.subscribePaymentMethods(
      currentUser.id,
      setPaymentMethods,
    );

    return () => {
      unsubscribePaymentMethods && unsubscribePaymentMethods();
    };
  }, []);

  const renderNavHeaderLeft = () => {
    return (
      <MenuIcon
        source={canGoBack ? AppStyles.iconSet.back : AppStyles.iconSet.menu}
        onPress={onMenuIconPress}
      />
    );
  };

  const onMenuIconPress = () => {
    if (canGoBack) {
      navigation.goBack();
    } else {
      navigation.openDrawer();
    }
  };

  const setStripeCustomerId = async () => {
    const customerID = await retrieveStripeCustomerId();
    stripeCustomerID.current = customerID;
  };

  const setPaymentMethods = (methods) => {
    dispatch(updatePaymentMethods(methods));
  };

  const removeFromPaymentMethods = async (method) => {
    const customerID = await retrieveStripeCustomerId();

    if (!customerID) {
      return;
    }

    const result = await paymentRequestAPI.current.deletePaymentSource(
      customerID,
      method.cardId,
    );

    if (result.data?.response?.deleted) {
      onRemoveFromPaymentMethods(method);
    }
  };

  const onRemoveFromPaymentMethods = (method) => {
    paymentMethodDataManager.current.deleteFromUserPaymentMethods(
      method.cardId,
    );
    if (method.key === currentUser.defaultPaymentKey) {
      const newSelectedPayment = paymentMethods[0];
      dispatch(setSelectedPaymentMethod(paymentMethods[0]));
      userAPIManager.updateUserData(currentUser.id, {
        defaultPaymentKey: newSelectedPayment.key,
      });

      dispatch(
        setUserData({
          user: { ...currentUser, defaultPaymentKey: newSelectedPayment.key },
        }),
      );
    }
  };

  const retrieveStripeCustomerId = async () => {
    if (stripeCustomerID.current) {
      return stripeCustomerID.current;
    }

    if (currentUser.stripeCustomerID) {
      return currentUser.stripeCustomerID;
    }

    const response = await paymentRequestAPI.current.getStripeCustomerID(
      currentUser.email,
    );

    if (response) {
      stripeCustomerID.current = response;
      userAPIManager.updateUserData(currentUser.id, {
        stripeCustomerID: response,
      });
      dispatch(
        setUserData({
          user: { ...currentUser, stripeCustomerID: response },
        }),
      );

      return stripeCustomerID.current;
    }
    return null;
  };

  const onAddNewCard = async () => {
    try {
      const token = await stripe.paymentRequestWithCardForm(options);

      addToken(token);
    } catch (err) {
      console.log(err);
      // alert("an error occured while trying to add card, please try again.");
      alert(err);
    }
  };

  const addToken = async (token) => {
    const customerID = await retrieveStripeCustomerId();
    if (!token) {
      return;
    }

    if (customerID) {
      const source = await paymentRequestAPI.current.addNewPaymentSource(
        customerID,
        token.tokenId,
      );

      onUpdatePaymentMethod(token, source);
    }
  };

  const onUpdatePaymentMethod = (token, source) => {
    if (source.success && source.data.response) {
      paymentMethodDataManager.current.updateUserPaymentMethods({
        ownerId: currentUser.id,
        card: token.card,
      });
      paymentMethodDataManager.current.savePaymentSource(
        currentUser.id,
        source.data.response,
      );
    } else {
      console.log(source);
      alert('An error occured, please try again.');
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.optionsTitleContainer}>
        <Text style={styles.optionsTitle}>{IMLocalized('Payment Method')}</Text>
      </View>
      <PaymentOptions
        onAddNewCard={onAddNewCard}
        onPaymentMethodLongPress={removeFromPaymentMethods}
      />
    </View>
  );
}
