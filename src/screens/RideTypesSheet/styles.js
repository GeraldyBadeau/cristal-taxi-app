import { StyleSheet } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    contentContainerStyle: {
      paddingTop: 12,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
  });
};

export default dynamicStyles;
