import { StyleSheet } from 'react-native';
import AppStyles from '../../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      padding: 20,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    selectedItemContainer: {
      backgroundColor: AppStyles.colorSet[colorScheme].grey2,
    },
    image: {
      height: 70,
      width: 80,
      resizeMode: 'contain',
    },
    middleContainer: {
      flex: 1,
      marginHorizontal: 10,
    },
    type: {
      fontWeight: 'bold',
      fontSize: 18,
      marginBottom: 5,
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    numberOfPassenger: {
      fontWeight: 'bold',
      fontSize: 12,
      marginBottom: 5,
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    accessibility: {
      marginLeft: 4,
      paddingLeft: 4,
    },
    time: {
      color: AppStyles.colorSet[colorScheme].mainSubtextColor,
    },
    rightContainer: {
      justifyContent: 'flex-end',
      flexDirection: 'row',
      maxWidth: '50%',
    },
    price: {
      fontWeight: 'bold',
      fontSize: 18,
      marginLeft: 5,
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
  });
};

export default dynamicStyles;
