import React, { useEffect, useState } from 'react';
import { View, Image, Text, Pressable } from 'react-native';
import dynamicStyles from './styles.js';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { useColorScheme } from 'react-native-appearance';
import {
  getRideETA,
  getCarImage,
  getRideEstimatedPrice,
  getCarType,
} from '../../../utils';

const RideTypeItem = (props) => {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const { dropoffETA, dropoffDistance, item, onPress, isSelected } = props;

  const [priceRange, setPriceRange] = useState('');

  useEffect(() => {
    const range = getRideEstimatedPrice(dropoffETA, dropoffDistance, item);
    if (range) {
      setPriceRange(range);
    }
  }, [dropoffETA]);

  return (
    <Pressable
      onPress={() => onPress(item, priceRange)}
      style={[styles.container, isSelected && styles.selectedItemContainer]}>
      <Image style={styles.image} source={getCarImage(item.type)} />

      <View style={styles.middleContainer}>
        <Text style={styles.type}>
          {getCarType(item.type)} {item.type !== 'uber_x' && <Ionicons name={'person'} size={12} /> }
          <Text style={styles.numberOfPassenger}>{item.type !=='uber_x' && item.numberOfPassenger} </Text>
          {item.type=='uber_x' && <FontAwesome5 style={styles.accessibility} name={'wheelchair'} solid />}
        </Text>
        <Text style={styles.time}>{getRideETA(dropoffETA, item)}</Text>
      </View>
      {/* <View style={styles.rightContainer}>
        <Text style={styles.price}>{priceRange}</Text>
      </View> */}
    </Pressable>
  );
};

export default RideTypeItem;
