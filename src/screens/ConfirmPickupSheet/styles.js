import { StyleSheet } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    container: {
      // flex: 1,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
      paddingTop: 12,
    },
    inputBox: {
      margin: 10,
      padding: 15,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    pickupTitleContainer: {
      width: '70%',
    },
    pickupTitle: {
      fontSize: 20,
      fontWeight: '600',
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    searchContainer: {
      backgroundColor: AppStyles.colorSet[colorScheme].grey0,
      height: 38,
      width: 100,
      borderRadius: 75,
      justifyContent: 'center',
      alignItems: 'center',
    },
    searchTitle: {
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
  });
};

export default dynamicStyles;
