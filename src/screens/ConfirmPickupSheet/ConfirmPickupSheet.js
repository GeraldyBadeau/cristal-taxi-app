import React, { useMemo, useCallback } from 'react';
import { View, Text, Pressable } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { BottomSheetView } from '@gorhom/bottom-sheet';
import { useIsFocused } from '@react-navigation/native';
import { useColorScheme } from 'react-native-appearance';
import { setbottomSheetSnapPoints } from '../../redux';
import dynamicStyles from './styles';
import { altBottomContainerHeight } from '../../components/BottomButton/styles';
import { IMLocalized } from '../../Core/localization/IMLocalization';

const ConfirmPickupSheet = (props) => {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const dispatch = useDispatch();

  const isFocus = useIsFocused();

  const { navigation } = props;

  const origin = useSelector(({ trip }) => trip.origin);

  const onSearch = (item) => {};

  const contentContainerStyle = useMemo(
    () => ({
      ...styles.container,
      paddingBottom: altBottomContainerHeight,
    }),
    [],
  );

  const onConfirmPickupSheet = () => {
    navigation.navigate('Search', { updatingOrigin: true });
  };

  const handleOnLayout = useCallback(
    ({
      nativeEvent: {
        layout: { height },
      },
    }) => {
      dispatch(
        setbottomSheetSnapPoints({
          key: 'confirm_pickup',
          snapPoints: [height, height],
          index: 0,
        }),
      );
    },
    [],
  );

  return (
    <BottomSheetView
      style={contentContainerStyle}
      onLayout={isFocus && handleOnLayout}>
      <Pressable onPress={onConfirmPickupSheet} style={styles.inputBox}>
        <View style={styles.pickupTitleContainer}>
          <Text numberOfLines={2} style={styles.pickupTitle}>
            {origin?.title ?? IMLocalized('Choose location')}
          </Text>
        </View>

        <View style={styles.searchContainer}>
          <Text style={styles.searchTitle}>{IMLocalized('Search')}</Text>
        </View>
      </Pressable>
    </BottomSheetView>
  );
};

export default React.memo(ConfirmPickupSheet);
