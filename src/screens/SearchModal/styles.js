import { StyleSheet } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    container: {
      padding: 10,
      height: '100%',
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    navHeaderContainer: {
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    textInput: {
      padding: 10,
      backgroundColor: AppStyles.colorSet[colorScheme].grey2,
      marginVertical: 5,
      marginLeft: 20,
      color: AppStyles.colorSet[colorScheme].secondarySubtextColor,
      borderRadius: 3
    },
    textInputContainer: {
      width: '100%',
    },
    separator: {
      backgroundColor: AppStyles.colorSet[colorScheme].grey2,
      height: 1,
    },
    listView: {
      position: 'absolute',
      top: 105,
      bottom: 0,
    },
    autocompleteContainer: {
      position: 'absolute',
      top: 0,
      left: 10,
      right: 10,
      bottom: 0,
    },
    locationItemContainer: {
      flexDirection: 'row',
      width: '100%',
      alignItems: 'center',
      marginVertical: 5,
      height: 64,
      // borderBottomColor: AppStyles.colorSet[colorScheme].grey1,
      // borderBottomWidth: 1,
    },
    locationTextContainer: {
      height: '100%',
      justifyContent: 'center',
    },
    mainLocationText: {
      paddingBottom: 4,
      fontWeight: '400',
      fontSize: 16,
      color: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    secondaryLocationText: {
      fontWeight: '400',
      color: AppStyles.colorSet[colorScheme].secondaryMaintextColor,
    },
    iconContainer: {
      backgroundColor: AppStyles.colorSet[colorScheme].secondarySubtextColor,
      padding: 5,
      borderRadius: 50,
      marginRight: 15,
    },
    circle: {
      width: 5,
      height: 5,
      backgroundColor: 'black',
      position: 'absolute',
      top: 20,
      left: 15,
      borderRadius: 5,
    },
    line: {
      width: 1,
      height: 50,
      backgroundColor: AppStyles.colorSet[colorScheme].grey4,
      position: 'absolute',
      top: 28,
      left: 17,
    },
    square: {
      width: 5,
      height: 5,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeForegroundColor,
      position: 'absolute',
      top: 80,
      left: 15,
    },
    tintIndicator: {
      backgroundColor: AppStyles.colorSet[colorScheme].blue,
    },
  });
};

export default dynamicStyles;
