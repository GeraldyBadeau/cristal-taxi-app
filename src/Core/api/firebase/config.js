import { decode, encode } from 'base-64';
import './timerConfig';
global.addEventListener = (x) => x;
if (!global.btoa) {
  global.btoa = encode;
}

if (!global.atob) {
  global.atob = decode;
}

import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyD6SOeG3sL7etC0ZucH6NMQqpMF8Mkxc6Q',
  authDomain: 'cristal-chauffeurs-bfefa.firebaseapp.com',
  projectId: 'cristal-chauffeurs-bfefa',
  storageBucket: 'cristal-chauffeurs-bfefa.appspot.com',
  messagingSenderId: '500110386820',
  appId: '1:500110386820:web:9fc08d94114868244dc4d1',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
  firebase.firestore().settings({
    experimentalForceLongPolling: true,
    ignoreUndefinedProperties: true,
  });
}

export { firebase };
