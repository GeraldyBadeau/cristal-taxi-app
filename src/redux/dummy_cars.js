exports.dummyCars = [
  {
    id: '0',
    type: 'uber_x',
    latitude: 48.450627,
    longitude: 2.263045,
    heading: 47,
  },
  {
    id: '1',
    type: 'comfort',
    latitude: 48.456312,
    longitude: 2.252929,
    heading: 190,
  },
  {
    id: '2',
    type: 'uber_xl',
    latitude: 48.456208,
    longitude: 2.259098,
    heading: 99,
  },
  {
    id: '3',
    type: 'comfort',
    latitude: 48.454812,
    longitude: 2.258658,
    heading: 120,
  },
];
