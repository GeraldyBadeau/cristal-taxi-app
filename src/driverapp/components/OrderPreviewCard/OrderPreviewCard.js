import React, { useState } from 'react';
import { Alert, Text, TouchableOpacity, View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { TNCard, TNActivityIndicator } from '../../../Core/truly-native';
import dynamicStyles from './styles';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import { DriverAPIManager } from '../../api/driver';
import { setUserData } from '../../../Core/onboarding/redux/auth';
import AppStyles from '../../../DynamicAppStyles';
import AppConfig from '../../DriverAppConfig';
import { firebase } from '../../../Core/api/firebase/config';

const apiManager = new DriverAPIManager();

const OrderPreviewCard = ({ order, driver, appStyles, onMessagePress }) => {
  const styles = dynamicStyles(appStyles);
  const dispatch = useDispatch();
  const currentUser = useSelector((state) => state.auth.user);

  const [loading, setLoading] = useState(false);

  const buttonTitle =
    order.status === 'driver_accepted'
      ? IMLocalized('Start Trip')
      : IMLocalized('Complete Trip');
      //TODO: trip_started ??
  const headlineText =
    order.status === 'driver_accepted'
      ? IMLocalized('Picking up - ') +
        order?.passenger?.firstName +
        ' ' +
        order?.passenger?.lastName
      : IMLocalized('Heading to ') + order?.dropoff?.title;
  const address =
    order.status === 'driver_accepted' ? order?.dropoff?.title : '';

  const onPress = () => {
    if (order.status === 'driver_accepted') {
      // Passenger has been picked up, so we update the status
      apiManager.markAsPickedUp(order);
    } else {
      completeTrip();
    }
  };

  const completeTrip = async () => {
    setLoading(true);
    let title = IMLocalized('Unable to complete trip');
    let description = IMLocalized('An error occured while trying to complete trip. Please try again.');
    
    // const timestamp = firebase.firestore.Timestamp.fromDate(new Date());
    // let maxTimestamp = 0;
    // maxTimestamp = order.tripStartTime+36000;
    // maxTimestamp = maxTimestamp / 1000 ;
    // console.log('timestamp: '+JSON.stringify(timestamp.seconds));
    // console.log('maxTimestamp: '+maxTimestamp);
    // if (maxTimestamp < timestamp.seconds) {
    //   apiManager.reject(order, driver);
    //   title = 'Commande annulée';
    //   description = 'La commande a été annulée car elle date de plus de 10h';
    // }
    // else {
      const rs = await apiManager.markAsCompleted(order, driver);

      //console.log('rs: '+JSON.stringify(rs));

      if (!rs.isPaymentCompleted && rs.price) {
        title = IMLocalized('Trip Completed');
        description = '';
        // description = `You will receive the amout of ${AppConfig.currency?.toUpperCase()}${
        //   rs.price
        // } for the trip fare`;
      }

      if (rs.isPaymentCompleted && rs.price) {
        title = 'Charged Successfully';
        description = `The cost of the trip is ${AppConfig.currency?.toUpperCase()}${
          rs.price
        } and the payment was successfully charged.`;
      }

      if (rs.price) {
        dispatch(
          setUserData({ user: { ...currentUser, inProgressOrderID: null } }),
        );
      }
    // }
    

    Alert.alert(IMLocalized(title), IMLocalized(description), [
      { text: 'Ok', onPress: () => setLoading(false) },
    ]);

  };

  return (
    <>
      <TNCard appStyles={appStyles} containerStyle={styles.container}>
        <View style={styles.contentView}>
          <View style={styles.textContainer}>
            <Text style={styles.headline}>{headlineText}</Text>
            <Text style={styles.description}>
              {IMLocalized('Order #')}
              {order.id}
            </Text>
            <Text style={styles.description}>{address}</Text>
          </View>
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              style={styles.actionButtonContainer}
              onPress={onPress}>
              <Text style={styles.actionButtonText}>{buttonTitle}</Text>
            </TouchableOpacity>
            {order.status !== 'awaiting_driver' && (
              <TouchableOpacity
                style={styles.secondaryButtonContainer}
                onPress={onMessagePress}>
                <Text style={styles.secondaryButtonText}>
                  {IMLocalized('Message')}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </TNCard>
      {loading && <TNActivityIndicator appStyles={AppStyles} />}
    </>
  );
};

export default OrderPreviewCard;
