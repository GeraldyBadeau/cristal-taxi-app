import React, { useRef, useEffect, useLayoutEffect, useState } from 'react';
import { FlatList, Text, View, TouchableOpacity } from 'react-native';
import Button from 'react-native-button';
import FastImage from 'react-native-fast-image';
import { useSelector } from 'react-redux';
import { Appearance } from 'react-native-appearance';
import PropTypes from 'prop-types';
import DynamicAppStyles from '../../../DynamicAppStyles';
import styles from './styles';
import Hamburger from '../../../components/Hamburger/Hamburger';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import { OrdersAPIManager } from '../../api/orders';
import {
  TNEmptyStateView,
  TNActivityIndicator,
} from '../../../Core/truly-native';

const COLOR_SCHEME = Appearance.getColorScheme();

const statusDescription = {
  trip_completed: 'Course terminée',
};

const OrdersScreen = (props) => {
  const { navigation } = props;

  const [orders, setOrders] = useState(null);

  const currentUser = useSelector((state) => state.auth.user);
  const apiManager = useRef(new OrdersAPIManager());

  const emptyStateConfig = {
    title: IMLocalized('Pas de course'),
    description: IMLocalized(
      'Vous n\avez pas encore réalisé de course. Toutes vos courses seront là.',
    ),
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      title: IMLocalized('Mes courses'),
      headerLeft: () => (
        <Hamburger
          onPress={() => {
            navigation.openDrawer();
          }}
        />
      ),
    });
  });

  useEffect(() => {
    apiManager.current.subscribe(currentUser.id, onOrdersUpdate);
    return apiManager.current.unsubscribe;
  }, []);

  const onOrdersUpdate = (data) => {
    setOrders(data);
  };

  const renderItem = ({ item }) => {
    return (
      <View style={styles.container}>
        <View>
          {item != null &&
            item.products != null &&
            item.products[0] != null &&
            item.products[0].photo != null &&
            item.products[0].photo.length > 0 && (
              <FastImage
                placeholderColor={DynamicAppStyles.colorSet[COLOR_SCHEME].grey9}
                style={styles.photo}
                source={{ uri: item.products[0].photo }}
              />
            )}
          <View style={styles.overlay} />
        </View>
        <View style={styles.routeContainer}>
          <View style={styles.routeIindicatorContainer}>
            <View style={styles.circle} />

            <View style={styles.line} />

            <View style={styles.square} />
          </View>
          <View style={styles.routeDescriptionContainer}>
            <Text numberOfLines={1} style={styles.routeTitle}>
              {item?.pickup?.title}
            </Text>
            <Text numberOfLines={1} style={styles.routeTitle}>
              {item?.dropoff?.title}
            </Text>
          </View>
        </View>
        <View style={styles.actionContainer}>
          <Text style={styles.total}>
            {/* {IMLocalized('Total: €')}
            {item.price} */}
          </Text>
          <Text style={styles.statusText}>
            {statusDescription[item.status]}
          </Text>
        </View>
      </View>
    );
  };

  if (orders == null) {
    return <TNActivityIndicator appStyles={DynamicAppStyles} />;
  }

  if (orders.length == 0) {
    return (
      <View style={styles.emptyViewContainer}>
        <TNEmptyStateView
          appStyles={DynamicAppStyles}
          emptyStateConfig={emptyStateConfig}
        />
      </View>
    );
  }

  return (
    <FlatList
      style={styles.flat}
      data={orders}
      showsVerticalScrollIndicator={false}
      renderItem={renderItem}
      keyExtractor={(item) => `${item.id}`}
      initialNumToRender={5}
    />
  );
};

OrdersScreen.propTypes = {
  user: PropTypes.shape(),
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }),
};

export default OrdersScreen;
