import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import { useColorScheme } from 'react-native-appearance';
import dynamicStyles from './styles';
import DynamicAppStyles from '../../DynamicAppStyles';

export default function Hamburger({ onPress }) {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  return (
    <TouchableOpacity style={styles.headerButtonContainer} onPress={onPress}>
      <Image
        style={styles.headerButtonImage}
        source={DynamicAppStyles.iconSet.menuHamburger}
      />
    </TouchableOpacity>
  );
}
