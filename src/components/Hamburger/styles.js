import { StyleSheet } from 'react-native';
import { Appearance } from 'react-native-appearance';
import AppStyles from '../../DynamicAppStyles';

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    headerButtonContainer: {
      padding: 10,
    },
    headerButtonImage: {
      justifyContent: 'center',
      width: 25,
      height: 25,
      margin: 6,
      tintColor: AppStyles.colorSet[colorScheme].secondaryMaintextColor,
    },
  });
};

export default dynamicStyles;
