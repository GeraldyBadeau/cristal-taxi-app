import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import dynamicStyles from './styles';
import AppStyles from '../../DynamicAppStyles';

export default function MenuIcon({ onPress, source, withShadow }) {
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.container, withShadow && styles.shadowBackground]}>
      <Image
        style={styles.icon}
        source={source ?? AppStyles.iconSet.menu}
        resizeMode={'contain'}
      />
    </TouchableOpacity>
  );
}
