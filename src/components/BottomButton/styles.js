import { StyleSheet } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

export const altBottomContainerHeight = 75;
export const bottomContainerHeight = 135;

const dynamicStyles = (colorScheme) => {
  return StyleSheet.create({
    container: {
      width: '100%',
      alignItems: 'center',
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    buttonContainer: {
      height: 55,
      width: '80%',
      backgroundColor: 'black',
      alignItems: 'center',
      alignSelf: 'center',
      justifyContent: 'center',
    },
    title: {
      color: 'white',
      fontWeight: '500',
      fontSize: 20,
    },
  });
};

export default dynamicStyles;
