import { StyleSheet } from 'react-native';
import AppStyles from '../../DynamicAppStyles';

const cardInputFontSize = 16;

const dynamicStyles = (colorScheme) => {
  return new StyleSheet.create({
    paymentMethodContainer: {
      flexDirection: 'row',
      borderBottomColor: AppStyles.colorSet[colorScheme].grey6,
      borderBottomWidth: 0.5,
      paddingLeft: 20,
      height: 60,
      backgroundColor: AppStyles.colorSet[colorScheme].mainThemeBackgroundColor,
    },
    methodIconContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    tickIcon: {
      height: 17,
      width: 17,
      tintColor: AppStyles.colorSet[colorScheme].mainTextColor,
    },
    paymentOptionIconContainer: {
      flex: 0.5,
      justifyContent: 'center',
      alignItems: 'center',
    },
    paymentOptionIcon: {
      height: 28,
      width: 28,
      // tintColor: AppStyles.colorSet[colorScheme].mainTextColor
    },
    optionDetailContainer: {
      flex: 4,
      justifyContent: 'center',
      marginVertical: 10,
    },
    optionTitle: {
      color: AppStyles.colorSet[colorScheme].mainSubtextColor,
      fontSize: cardInputFontSize,
      paddingLeft: 7,
    },
  });
};

export default dynamicStyles;
