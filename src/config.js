import { Platform } from 'react-native';
import { IMLocalized, setI18nConfig } from './Core/localization/IMLocalization';
import DynamicAppStyles from './DynamicAppStyles';

setI18nConfig();

const regexForNames = /^[a-zA-Z]{2,25}$/;
const regexForPhoneNumber = /\d{9}$/;

const TaxiConfig = {
  defaultProfilePhotoURL:
    'https://www.iosapptemplates.com/wp-content/uploads/2019/06/empty-avatar.jpg',
  defaultCarAvatar:
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZh2MZ2vQUMd6kXVGsTEEcrbgr4DAhbnG88A&usqp=CAU',
  googleMapsApikey: 'AIzaSyCpZQUM-RJgyfkf44w_Tsc3p_CiaWbEM7M',
  isSMSAuthEnabled: false,
  appIdentifier: 'rn-taxi-app-android',
  onboardingConfig: {
    welcomeTitle: IMLocalized('Cristal Chauffeurs'),
    welcomeCaption: IMLocalized(
      'Commandez une voiture, suivez votre course et notez votre chauffeur.',
    ),
    walkthroughScreens: [
      {
        icon: require('../assets/booktaxi.png'),
        title: IMLocalized('Commandez une Voiture'),
        description: IMLocalized(
          'Choisissez un lieu d\'enlèvement et une destination pour votre course, un chauffeur viendra vous chercher.',
        ),
      },
      {
        icon: require('../assets/maptrack.png'),
        title: IMLocalized('Suivez votre Course'),
        description: IMLocalized(
          'Visualisez le chauffeur sur une carte interactive et suivez votre course.',
        ),
      },
      {
        icon: require('../assets/star.png'),
        title: IMLocalized('Notez votre chauffeur'),
        description: IMLocalized(
          'Notez votre chauffeur après chaque course pour améliorer la qualité de vos courses.',
        ),
      },
      {
        icon: require('../assets/triphistory.png'),
        title: IMLocalized('Historique'),
        description: IMLocalized(
          'Consultez instantanément l\'historique complet de vos courses.',
        ),
      },
    ],
  },
  drawerMenu: {
    forceHeaderStyle: {
      backgroundColor: '#000',
      marginTop: 0,
      height: Platform.OS === 'ios' ? '25%' : '31%',
    },
    forceNameStyle: {
      color: '#fff',
    },
    forceEmailStyle: {
      color: '#fff',
    },
    forceMenuItemsStyle: {
      // marginLeft: 0,
    },
    forceMenuItemStyle: {
      fontWeight: '400',
      fontSize: 18,
      // marginLeft: 10,
      marginTop: 5,
    },
    upperMenu: [
      {
        title: IMLocalized('Home'),
        // icon:
        //   Platform.OS === 'ios'
        //     ? DynamicAppStyles.iconSet.home
        //     : DynamicAppStyles.iconSet.home_android,
        navigationPath: 'Home',
      },
      {
        title: IMLocalized('Your Trips'),
        navigationPath: 'Trips',
      },
      // {
      //   title: IMLocalized('Wallet'),
      //   navigationPath: 'Wallet',
      // },
      {
        title: IMLocalized('Profile'),
        navigationPath: 'Profile',
      },
    ],
    lowerMenu: [
      {
        title: IMLocalized('LOG OUT'),
        icon: DynamicAppStyles.iconSet.shutdown,
        action: 'logout',
      },
    ],
  },
  tosLink: 'https://www.instamobile.io/eula-instachatty/',
  isUsernameFieldEnabled: false,
  smsSignupFields: [
    {
      displayName: IMLocalized('First Name'),
      type: 'ascii-capable',
      editable: true,
      regex: regexForNames,
      key: 'firstName',
      placeholder: 'First Name',
    },
    {
      displayName: IMLocalized('Last Name'),
      type: 'ascii-capable',
      editable: true,
      regex: regexForNames,
      key: 'lastName',
      placeholder: 'Last Name',
    },
  ],
  signupFields: [
    {
      displayName: IMLocalized('First Name'),
      type: 'ascii-capable',
      editable: true,
      regex: regexForNames,
      key: 'firstName',
      placeholder: 'First Name',
    },
    {
      displayName: IMLocalized('Last Name'),
      type: 'ascii-capable',
      editable: true,
      regex: regexForNames,
      key: 'lastName',
      placeholder: 'Last Name',
    },
    {
      displayName: IMLocalized('E-mail Address'),
      type: 'email-address',
      editable: true,
      regex: regexForNames,
      key: 'email',
      placeholder: 'E-mail Address',
      autoCapitalize: 'none',
    },
    {
      displayName: IMLocalized('Password'),
      type: 'default',
      secureTextEntry: true,
      editable: true,
      regex: regexForNames,
      key: 'password',
      placeholder: 'Password',
      autoCapitalize: 'none',
    },
  ],
  editProfileFields: {
    sections: [
      {
        title: IMLocalized('PUBLIC PROFILE'),
        fields: [
          {
            displayName: IMLocalized('First Name'),
            type: 'text',
            editable: true,
            regex: regexForNames,
            key: 'firstName',
            placeholder: 'Your first name',
          },
          {
            displayName: IMLocalized('Last Name'),
            type: 'text',
            editable: true,
            regex: regexForNames,
            key: 'lastName',
            placeholder: 'Your last name',
          },
        ],
      },
      {
        title: IMLocalized('PRIVATE DETAILS'),
        fields: [
          {
            displayName: IMLocalized('E-mail Address'),
            type: 'text',
            editable: true,
            key: 'email',
            placeholder: 'Votre adresse email',
          },
          {
            displayName: IMLocalized('Phone Number'),
            type: 'text',
            editable: true,
            regex: regexForPhoneNumber,
            key: 'phone',
            placeholder: 'Votre téléphone',
          },
        ],
      },
    ],
  },
  userSettingsFields: {
    sections: [
      {
        title: IMLocalized('GENERAL'),
        fields: [
          {
            displayName: IMLocalized('Allow Push Notifications'),
            type: 'switch',
            editable: true,
            key: 'push_notifications_enabled',
            value: true,
          },
          {
            ...(Platform.OS === 'ios'
              ? {
                  displayName: IMLocalized('Enable Face ID / Touch ID'),
                  type: 'switch',
                  editable: true,
                  key: 'face_id_enabled',
                  value: false,
                }
              : {}),
          },
        ],
      },
      {
        title: '',
        fields: [
          {
            displayName: IMLocalized('Save'),
            type: 'button',
            key: 'savebutton',
          },
        ],
      },
    ],
  },
  contactUsFields: {
    sections: [
      {
        title: IMLocalized('CONTACT'),
        fields: [
          {
            displayName: IMLocalized('Address'),
            type: 'text',
            editable: false,
            key: 'push_notifications_enabled',
            value: '6 rue du gué, 77122, Monthyon',
          },
          {
            displayName: IMLocalized('E-mail us'),
            value: 'contact@cristalchauffeurs.fr',
            type: 'text',
            editable: false,
            key: 'email',
            placeholder: 'Your e-mail address',
          },
        ],
      },
      {
        title: '',
        fields: [
          {
            displayName: IMLocalized('Call Us'),
            type: 'button',
            key: 'savebutton',
          },
        ],
      },
    ],
  },
  drawerMenuConfig: {
    adminDrawerConfig: {
      upperMenu: [
        {
          title: IMLocalized('HOME'),
          icon: DynamicAppStyles.iconSet.shop,
          navigationPath: 'Restaurants',
        },
        {
          title: IMLocalized('ORDERS'),
          icon: DynamicAppStyles.iconSet.delivery,
          navigationPath: 'AdminOrder',
        },
        {
          title: IMLocalized('DELIVERY'),
          icon: DynamicAppStyles.iconSet.delivery,
          navigationPath: 'Map',
        },
      ],
      lowerMenu: [
        {
          title: IMLocalized('LOG OUT'),
          icon: DynamicAppStyles.iconSet.shutdown,
          action: 'logout',
        },
      ],
    },
    vendorDrawerConfig: {
      upperMenu: [
        {
          title: IMLocalized('HOME'),
          icon: DynamicAppStyles.iconSet.shop,
          navigationPath: 'Home',
        },
        {
          title: IMLocalized('CUISINES'),
          icon: DynamicAppStyles.iconSet.menu,
          navigationPath: 'CategoryList',
        },
        {
          title: IMLocalized('SEARCH'),
          icon: DynamicAppStyles.iconSet.search,
          navigationPath: 'Search',
        },
        {
          title: IMLocalized('CART'),
          icon: DynamicAppStyles.iconSet.cart,
          navigationPath: 'Cart',
        },
        {
          title: IMLocalized('RESERVATIONS'),
          icon: DynamicAppStyles.iconSet.reserve,
          navigationPath: 'ReservationHistoryScreen',
        },
        {
          title: IMLocalized('PROFILE'),
          icon: DynamicAppStyles.iconSet.profile,
          navigationPath: 'MyProfile',
        },
        {
          title: IMLocalized('ORDERS'),
          icon: DynamicAppStyles.iconSet.delivery,
          navigationPath: 'OrderList',
        },
      ],
      lowerMenu: [
        {
          title: IMLocalized('LOG OUT'),
          icon: DynamicAppStyles.iconSet.shutdown,
          action: 'logout',
        },
      ],
    },
    customerDrawerConfig: {
      upperMenu: [
        {
          title: IMLocalized('HOME'),
          icon: DynamicAppStyles.iconSet.shop,
          navigationPath: 'Home',
        },
        {
          title: IMLocalized('CUISINES'),
          icon: DynamicAppStyles.iconSet.menu,
          navigationPath: 'CategoryList',
        },
        {
          title: IMLocalized('SEARCH'),
          icon: DynamicAppStyles.iconSet.search,
          navigationPath: 'Search',
        },
        {
          title: IMLocalized('CART'),
          icon: DynamicAppStyles.iconSet.cart,
          navigationPath: 'Cart',
        },
        {
          title: IMLocalized('PROFILE'),
          icon: DynamicAppStyles.iconSet.profile,
          navigationPath: 'MyProfile',
        },
        {
          title: IMLocalized('ORDERS'),
          icon: DynamicAppStyles.iconSet.delivery,
          navigationPath: 'OrderList',
        },
      ],
      lowerMenu: [
        {
          title: IMLocalized('LOG OUT'),
          icon: DynamicAppStyles.iconSet.shutdown,
          action: 'logout',
        },
      ],
    },
    vendorDrawer: {
      upperMenu: [
        {
          title: IMLocalized('MANAGE ORDERS'),
          icon: DynamicAppStyles.iconSet.shop,
          navigationPath: 'Home',
        },
        {
          title: IMLocalized('MANAGE PRODUCTS'),
          icon: DynamicAppStyles.iconSet.foods,
          navigationPath: 'Products',
        },
      ],
      lowerMenu: [
        {
          title: IMLocalized('LOG OUT'),
          icon: DynamicAppStyles.iconSet.shutdown,
          action: 'logout',
        },
      ],
    },
    driverDrawerConfig: {
      upperMenu: [
        {
          title: IMLocalized('Home'),
          icon: DynamicAppStyles.iconSet.shop,
          navigationPath: 'DriverHome',
        },
        {
          title: IMLocalized('ORDERS'),
          icon: DynamicAppStyles.iconSet.delivery,
          navigationPath: 'OrderList',
        },
        {
          title: IMLocalized('PROFILE'),
          icon: DynamicAppStyles.iconSet.profile,
          navigationPath: 'MyProfile',
        },
      ],
      lowerMenu: [
        {
          title: IMLocalized('LOG OUT'),
          icon: DynamicAppStyles.iconSet.shutdown,
          action: 'logout',
        },
      ],
    },
  },
  contactUsPhoneNumber: '+16504850000',
  facebookIdentifier: '285315185217069',
  webClientId:
    '525472070731-mg8m3q8v9vp1port7nkbq9le65hp917t.apps.googleusercontent.com',
  GOOGLE_SIGNIN_CONFIG: {
    SCOPES: ['https://www.googleapis.com/auth/drive.photos.readonly'],
    WEB_CLIENT_ID:
      '706061484183-l0l58dds4kg329fh1trbiha1ci5rqm5n.apps.googleusercontent.com', // from google-services.json file
    OFFLINE_ACCESS: true,
  },
  FIREBASE_COLLECTIONS: {
    USERS: 'users',
    PAYMENT_METHODS: 'payment_methods',
    STRIPE_CUSTOMERS: 'stripe_customers',
    CATEGORIES: 'vendor_categories',
    CHARGES: 'charges',
    ORDERS: 'restaurant_orders',
    SOURCES: 'sources',
    PRODUCTS: 'vendor_products',
    SHIPPING_METHODS: 'shipping_methods',
  },
  stripeEnv: {
    API: {
      baseURL: 'https://murmuring-caverns-94283.herokuapp.com/', //your copied heroku link
      timeout: 30000,
    },
  },
  STRIPE_CONFIG: {
    PUBLISHABLE_KEY: 'pk_test_LSo5mTIQqkRiTWd0eBMSDAXf00QZGCttt3', // "pk_test_..." in test mode and ""pk_live_..."" in live mode
    MERCHANT_ID: 'Your_merchant_id_goes_here',
    ANDROID_PAYMENT_MODE: 'test', // test || production
  },
  displayCurrencyTitle: '€',
  currency: 'eur',
};

export default TaxiConfig;
