import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import IMDrawerMenu from '../Core/ui/drawer/IMDrawerMenu/IMDrawerMenu';
import {
  HomeNavigator,
  TripsNavigator,
  WalletNavigator,
  ProfileNavigator,
} from './InnerStackNavigators';
import { authManager } from '../Core/onboarding/utils/api/';
import DynamicAppStyles from '../DynamicAppStyles';
import AppConfig from '../config';
import { useSelector } from 'react-redux';

const DrawerStack = createStackNavigator();

const DrawerStackNavigator = () => {
  return (
    <DrawerStack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
      }}
      initialRouteName="Home">
      <DrawerStack.Screen
        name={'Home'}
        options={{
          headerShown: false,
        }}
        component={HomeNavigator}
      />
      <DrawerStack.Screen
        name={'Trips'}
        options={{
          headerShown: false,
        }}
        component={TripsNavigator}
      />
      <DrawerStack.Screen
        name={'Wallet'}
        options={{
          headerShown: false,
        }}
        component={WalletNavigator}
      />
      <DrawerStack.Screen
        name={'Profile'}
        options={{
          headerShown: false,
        }}
        component={ProfileNavigator}
      />
    </DrawerStack.Navigator>
  );
};

const Drawer = createDrawerNavigator();

const DrawerNavigator = (props) => {
  return (
    <Drawer.Navigator
      drawerPosition="left"
      drawerStyle={{ width: 310 }}
      initialRouteName={'DrawerMain'}
      screenOptions={{ headerShown: false }}
      drawerContent={({ navigation, state }) => {
        return (
          <IMDrawerMenu
            navigation={navigation}
            headerStyle={AppConfig.drawerMenu.forceHeaderStyle}
            nameStyle={AppConfig.drawerMenu.forceNameStyle}
            emailStyle={AppConfig.drawerMenu.forceEmailStyle}
            forceMenuItemsStyle={AppConfig.drawerMenu.forceMenuItemsStyle}
            menuItemStyle={AppConfig.drawerMenu.forceMenuItemStyle}
            menuItems={AppConfig.drawerMenu.upperMenu}
            menuItemsSettings={AppConfig.drawerMenu.lowerMenu}
            //   menuItems={AppConfig.drawerMenuConfig.driverDrawerConfig.upperMenu}
            // menuItemsSettings={
            //   AppConfig.drawerMenuConfig.driverDrawerConfig.lowerMenu
            // }
            appStyles={DynamicAppStyles}
            authManager={authManager}
            appConfig={AppConfig}
          />
        );
      }}>
      <Drawer.Screen name="DrawerMain" component={DrawerStackNavigator} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
