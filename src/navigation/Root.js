import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import stripe from 'tipsi-stripe';
import { LoadScreen, WalkthroughScreen } from '../Core/onboarding';
import MainStackNavigator from './MainStackNavigator';
import LoginStack from './AuthStackNavigator';
import AppStyles from '../DynamicAppStyles';
import AppConfig from '../config';

stripe.setOptions({
  publishableKey: AppConfig.STRIPE_CONFIG.PUBLISHABLE_KEY,
  merchantId: AppConfig.STRIPE_CONFIG.MERCHANT_ID,
  androidPayMode: AppConfig.STRIPE_CONFIG.ANDROID_PAYMENT_MODE,
});

const RootStack = createStackNavigator();

const RootNavigator = (props) => {
  return (
    <NavigationContainer>
      <RootStack.Navigator
        screenOptions={{ headerShown: false, animationEnabled: false }}
        initialRouteName="LoadScreen">
        <RootStack.Screen
          initialParams={{
            appStyles: AppStyles,
            appConfig: AppConfig,
          }}
          name="LoadScreen"
          component={LoadScreen}
        />
        <RootStack.Screen name="Walkthrough" component={WalkthroughScreen} />
        <RootStack.Screen name="LoginStack" component={LoginStack} />
        <RootStack.Screen name="MainStack" component={MainStackNavigator} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigator;
