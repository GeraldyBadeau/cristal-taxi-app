import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import DrawerNavigator from './DrawerNavigator';
import DriverDrawerNavigator from './DriverDrawerNavigator';
import { IMChatScreen } from '../Core/chat';
import WalletScreen from '../screens/WalletScreen/WalletScreen';
import SavePlaceScreen from '../screens/SavePlaceScreen/SavePlaceScreen';
import RatingsScreen from '../screens/RatingsScreen/RatingsScreen';
import { IMLocalized } from '../Core/localization/IMLocalization';
import { useSelector } from 'react-redux';

const cardStyleInterpolator = ({ current, layouts }) => {
  return {
    cardStyle: {
      transform: [
        {
          translateY: current.progress.interpolate({
            inputRange: [0, 1],
            outputRange: [layouts.screen.height, 0],
          }),
        },
      ],
    },
  };
};

const MainStack = createStackNavigator();
const MainStackNavigator = () => {
  const currentUser = useSelector((state) => state.auth.user);
  return (
    <MainStack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        headerBackTitle: IMLocalized('Back'),
      }}
      initialRouteName="Main">
      {currentUser?.role === 'driver' ? (
        <MainStack.Screen
          name={'Main'}
          options={{
            headerShown: false,
          }}
          component={DriverDrawerNavigator}
        />
      ) : (
        <MainStack.Screen
          name={'Main'}
          options={{
            headerShown: false,
          }}
          component={DrawerNavigator}
        />
      )}
      <MainStack.Screen name="PersonalChat" component={IMChatScreen} />
      <MainStack.Screen
        name="Ratings"
        options={{
          headerShown: false,
          gestureEnabled: false,
        }}
        component={RatingsScreen}
      />
      <MainStack.Screen
        name={'ChooseWallet'}
        options={{
          cardStyleInterpolator,
          gestureDirection: 'vertical',
          title: 'Change Wallet',
        }}
        component={WalletScreen}
      />
      <MainStack.Screen
        name={'SavePlace'}
        options={{
          title: 'Save place',
        }}
        component={SavePlaceScreen}
      />
    </MainStack.Navigator>
  );
};

export default MainStackNavigator;
