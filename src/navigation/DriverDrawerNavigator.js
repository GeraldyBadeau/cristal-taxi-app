import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { useColorScheme } from 'react-native-appearance';
import IMDrawerMenu from '../Core/ui/drawer/IMDrawerMenu/IMDrawerMenu';
import DriverHomeScreen from '../driverapp/screens/Home/HomeScreen';
import ProfileScreen from '../screens/ProfileScreen/ProfileScreen';
import DriverOrdersScreen from '../driverapp/screens/Orders/OrdersScreen';
import { authManager } from '../Core/onboarding/utils/api';
import { IMChatScreen } from '../Core/chat';
import {
  IMEditProfileScreen,
  IMUserSettingsScreen,
  IMContactUsScreen,
} from '../Core/profile';
import DynamicAppStyles from '../DynamicAppStyles';
import DriverAppConfig from '../driverapp/DriverAppConfig';
import AppConfig from '../config';

const DrawerStack = createStackNavigator();

const DrawerStackNavigator = () => {
  const colorScheme = useColorScheme();
  const currentTheme = DynamicAppStyles.navThemeConstants[colorScheme];
  return (
    <DrawerStack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
        // headerBackTitle: IMLocalized('Back'),
        headerStyle: {
          backgroundColor: currentTheme.backgroundColor,
        },
        headerTitleStyle: {
          color: currentTheme.fontColor,
        },
      }}
      initialRouteName="Home">
      <DrawerStack.Screen name={'DriverHome'} component={DriverHomeScreen} />
      <DrawerStack.Screen
        name={'MyProfile'}
        component={ProfileScreen}
        initialParams={{
          appConfig: DriverAppConfig,
        }}
      />
      <DrawerStack.Screen name={'OrderList'} component={DriverOrdersScreen} />
      <DrawerStack.Screen name={'ContactUs'} component={IMContactUsScreen} />
      <DrawerStack.Screen name={'Settings'} component={IMUserSettingsScreen} />
      <DrawerStack.Screen
        name={'AccountDetails'}
        component={IMEditProfileScreen}
      />
      <DrawerStack.Screen name={'PersonalChat'} component={IMChatScreen} />
    </DrawerStack.Navigator>
  );
};

const DriverMain = createStackNavigator();
const DriverMainNavigation = () => {
  const colorScheme = useColorScheme();
  return (
    <DriverMain.Navigator initialRouteName="Main">
      <DriverMain.Screen
        name="Main"
        options={{ headerShown: false }}
        component={DrawerStackNavigator}
      />
    </DriverMain.Navigator>
  );
};

const DriverDrawer = createDrawerNavigator();
const DriverDrawerNavigator = () => {
  return (
    <DriverDrawer.Navigator
      initialRouteName="DrawerMain"
      screenOptions={{ headerShown: false }}
      drawerContent={({ navigation }) => (
        <IMDrawerMenu
          navigation={navigation}
          menuItems={AppConfig.drawerMenuConfig.driverDrawerConfig.upperMenu}
          menuItemsSettings={
            AppConfig.drawerMenuConfig.driverDrawerConfig.lowerMenu
          }
          appStyles={DynamicAppStyles}
          authManager={authManager}
          appConfig={DriverAppConfig}
        />
      )}
      drawerPosition="left"
      drawerStyle={{ width: 250 }}>
      <DriverDrawer.Screen name="DrawerMain" component={DriverMainNavigation} />
    </DriverDrawer.Navigator>
  );
};

export default DriverDrawerNavigator;
