import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { StatusBar, PermissionsAndroid, Platform } from 'react-native';
import { Provider } from 'react-redux';
import Geolocation from '@react-native-community/geolocation';
import { BottomSheetModalProvider } from '@gorhom/bottom-sheet';
import { Appearance, AppearanceProvider } from 'react-native-appearance';
import { setI18nConfig } from './src/Core/localization/IMLocalization';
import configureStore from './src/redux/store';
import RootNavigator from './src/navigation/Root';

navigator.geolocation = require('@react-native-community/geolocation');
const store = configureStore();

const MainNavigator = RootNavigator;

const App = () => {
  const [colorScheme, setColorScheme] = useState(Appearance.getColorScheme());

  useEffect(() => {
    console.disableYellowBox = true;
    setI18nConfig();
    // RNLocalize.addEventListener("change", handleLocalizationChange);
    Appearance.addChangeListener(({ colorScheme }) => {
      setColorScheme(colorScheme);
    });
    return () => {
      // RNLocalize.removeEventListener("change", handleLocalizationChange);
    };
  }, []);

  useEffect(() => {
    if (Platform.OS === 'android') {
      androidPermission();
    } else {
      // IOS
      Geolocation.requestAuthorization();
    }
  }, []);

  const androidPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'TaxiApp Location Permission',
          message:
            'TaxiApp needs access to your location ' +
            'so you can take awesome rides.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the location');
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  return (
    <Provider store={store}>
      <BottomSheetModalProvider>
        <StatusBar barStyle="dark-content" />
        <MainNavigator />
      </BottomSheetModalProvider>
    </Provider>
  );
};

export default App;
